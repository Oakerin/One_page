# One page based on susy

## Commands
- `npm run dev` start lite-server
- `npm run scss-w` start sass watcher

## Packages
- [Lite-Server](https://www.npmjs.com/package/lite-server/ "Go ahead") dev server
- [Node-sass](https://www.npmjs.com/package/node-sass "Go ahead") is used to compile sass -> css 
- [Nodemon](https://www.npmjs.com/package/node-sass "Go ahead") is used like a watcher to sass changes